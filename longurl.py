import falcon
import hirlite
import hashlib
import random

longurlhost = "http://match-mayday.codio.io:3000/"
rlite = hirlite.Rlite(encoding='utf8',path='longurl.rld')
with open('genurl.html', 'r') as genurlFile:
  genurlHTML = genurlFile.read()

with open('genfriend.html', 'r') as genfriendFile:
  genfriendHTML = genfriendFile.read()

def insertURL(urlToRedir):
  randomGen = random.SystemRandom()
  urlToStore = hashlib.sha512((str(randomGen.random()) + urlToRedir + str(randomGen.random())).encode('utf-8')).hexdigest()
  global rlite
  rlite.command('set', urlToStore, urlToRedir)
  return urlToStore

class IndexPage:
  def on_get(self, req, resp):
    resp.status = falcon.HTTP_200
    resp.content_type = "text/html"
    resp.body = ('''<!DOCTYPE html><html><head><title>sgoblin's long url generator</title></head><body>Look for more documentation soon!</body></html>''')
    
class GenURL:
  def on_get(self, req, resp):
    resp.status = falcon.HTTP_400
    resp.content_type = "text/html"
    resp.body = genurlHTML
    
  def on_post(self, req, resp):
    requestBody = req.stream.read().decode('utf-8')
    print(req.content_type)
    urlThatWasStored = insertURL(requestBody)
    resp.status = falcon.HTTP_200
    resp.content_type = 'text/plain'
    resp.body = longurlhost + 'redir?url=' + urlThatWasStored
    
class GenFriend:
  def on_get(self, req, resp):
    resp.status = falcon.HTTP_200
    resp.content_type = "text/html"
    resp.body = genfriendHTML
    
  def on_post(self, req, resp):
    print(req.get_param('url'))
    urlThatWasStored = insertURL(req.get_param('url'))
    resp.status = falcon.HTTP_200
    resp.content_type = "text/plain"
    resp.body = 'Your longurl is: ' + longurlhost + 'redir?url=' + urlThatWasStored + "\nand was generated from: " + req.get_param('url')
    
class RedirURL:
  def on_get(self, req, resp):
    queryString = falcon.util.uri.parse_query_string(req.query_string)
    
    try:
      resp.status = falcon.HTTP_302
      print(queryString['url'])
      resp.location = rlite.command('get', queryString['url'])
    except:
      resp.status = falcon.HTTP_400
      resp.content_type = "text/html"
      resp.body = ('''<!DOCTYPE html><html><head><title>sgoblin's long url generator</title></head><body>There should be a query string on here or the query string is invalid.</body></html>''')
  def on_head(self, req, resp):
    queryString = falcon.util.uri.parse_query_string(req.query_string)
    try:
      resp.status = falcon.HTTP_307
      print(queryString['url'])
      resp.location = rlite.command('get', queryString['url'])
    except:
      resp.status = falcon.HTTP_400
      resp.content_type = "text/html"
      
app = falcon.API()

index = IndexPage()
genurl = GenURL()
redirurl = RedirURL()
genfriend = GenFriend()

app.add_route('/', index)
app.add_route('/gen', genurl)
app.add_route('/redir', redirurl)
app.add_route('/genform', genfriend)